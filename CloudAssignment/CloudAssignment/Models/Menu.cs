﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Models {
    public class Menu {
        public string Url { get; set; }
        public string Title { get; set; }
    }
}
