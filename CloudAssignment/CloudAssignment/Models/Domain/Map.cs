﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.Models.Domain {
    public class Map {
        [Required]
        public string Type { get; set; }
        [Required]
        public string AddressFrom { get; set; }
        [Required]
        public string AddressTo { get; set; }
    }
}
