﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;
using Google.Cloud.SecretManager.V1;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CloudAssignment.Controllers {
    [Authorize(Roles = "DRIVER")]
    public class ServiceController : Controller {
        private readonly IPubSubRepository _pubsubRepo;
        private readonly IConfiguration _config;
        private readonly ICarRepository _carRepo;
        private readonly ISubbedBookingsRepository _bookingRepo;


        public ServiceController(IPubSubRepository pubsubRepo, IConfiguration config, ICarRepository carRepo, ISubbedBookingsRepository bookingRepo) {
            _config = config;
            _carRepo = carRepo;
            _pubsubRepo = pubsubRepo;
            _bookingRepo = bookingRepo;
        }

        // GET: ServiceController
        public IActionResult Index() {
            string driverIdentity = HttpContext.User.Identity.Name;
            List<string> myTypes = _carRepo.GetCarsByOwner(driverIdentity);

            //return Content(myTypes.ToString());
            List<Booking> messages = new List<Booking>();
            int x = 0;
            foreach (string servicetype in myTypes) {
                x++;
                _pubsubRepo.PullServices(servicetype).ForEach(j => {
                    messages.Add(j);
                });

            }

            return View(messages);
        }

        public string GetMailGunApi() {
            SecretManagerServiceClient client = SecretManagerServiceClient.Create();
            SecretVersionName secretVersionName = new SecretVersionName("cloudassignment-308114", "CloudMailGun", "1");
            AccessSecretVersionResponse result = client.AccessSecretVersion(secretVersionName);
            String payload = result.Payload.Data.ToStringUtf8();

            return payload;

        }

        public IActionResult Submit(string AckId, string cat, string email) {
            _pubsubRepo.AcknowledgeMessage(AckId, cat);
            DriverCar car = _carRepo.GetCarsByOwnerAndType(HttpContext.User.Identity.Name, cat);
            SubbedBookings cb = new SubbedBookings();

            if(SendEmail(email, car)) {
                cb.ServiceType = cat;
                cb.DriverEmail = HttpContext.User.Identity.Name;
                cb.PassengerEmail = email;
                cb.CarId = car.CarId;

                _bookingRepo.AddConfirmedBooking(cb);
                TempData["message"] = $"Service is accepted.";
            }

            return RedirectToAction("");
        }

        public bool SendEmail(string recipient, DriverCar car) {
            try{
                HttpClient client = new HttpClient();
                var url = new Uri("https://us-central1-cloudassignment-308114.cloudfunctions.net/email-function?recipient=" + recipient);

                var clientByUrl = client.GetAsync(url);
                clientByUrl.Wait();
                Task<string> stringb = clientByUrl.Result.Content.ReadAsStringAsync();
                stringb.Wait();
                string msg = stringb.Result;
                string potoUrl = car.PhotoUrl;
                callF(potoUrl, recipient);
                sendMailGunEmail(recipient, potoUrl);
                return true;
            }catch(Exception e) {
                return false;
            }
        }

        public IActionResult callF(string url,string recipient) {
            HttpClient client = new HttpClient();
            Task<string> stringb = client.GetStringAsync("https://us-central1-cloudassignment-308114.cloudfunctions.net/email-function?recipient=" + recipient+"&carurl="+url);
            stringb.Wait();
            return Content(stringb.Result);
        }

        public string sendMailGunEmail(string email, string photoUrl) {
            RestClient rclient = new RestClient();
            rclient.BaseUrl = new Uri("https://api.mailgun.net/v3");
            string mailgunKey = GetMailGunApi();
            rclient.Authenticator = new HttpBasicAuthenticator("api", mailgunKey);
            RestRequest request = new RestRequest();
            request.AddParameter("domain", "sandboxfab3944c77624425a6f328f0592cad1e.mailgun.org", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter("from", "isaacdalli98@gmail.com");
            request.AddParameter("to", email);
            request.AddParameter("subject", "Receipt");
            request.AddParameter("text", "This is to acknowledge that your service has been accepted. " + photoUrl);
            request.Method = Method.POST;
            var response = rclient.Execute(request);
            return response.ToString();
        }




        //public IActionResult Submit(string AckId, string cat, string email) {
        //    Console.WriteLine(AckId);
        //    Console.WriteLine(cat);
        //    _pubsubRepo.AcknowledgeMessage(AckId, cat);

        //    DriverCar car = _carRepo.GetCarsByOwnerAndType(HttpContext.User.Identity.Name, cat);

        //    SubbedBookings cb = new SubbedBookings();

        //    RestClient client = new RestClient();
        //    client.BaseUrl = new Uri("https://api.mailgun.net/v3");

        //    string apiKey = GetMailGunApi();

        //    client.Authenticator = new HttpBasicAuthenticator("api", apiKey);
        //    RestRequest request = new RestRequest();
        //    request.AddParameter("domain", "sandbox6da842bc589445a5acdfe2a8e801f1d7.mailgun.org", ParameterType.UrlSegment);
        //    request.Resource = "{domain}/messages";
        //    request.AddParameter("from", "isaacdalli98@gmail.com");
        //    request.AddParameter("to", email);
        //    request.AddParameter("subject", "Receipt");
        //    request.AddParameter("text", "This is to acknowledge that your service has been accepted. " + car.PhotoUrl);
        //    request.Method = Method.POST;
        //    var response = client.Execute(request);

        //    cb.ServiceType = cat;
        //    cb.DriverEmail = HttpContext.User.Identity.Name;
        //    cb.PassengerEmail = email;
        //    cb.CarId = car.CarId;

        //    //add metohod to store details into firestore
        //    _bookingRepo.AddConfirmedBooking(cb);

        //    return RedirectToAction("");
        //}


    }
}
