﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Repositries {
    public class TypeRepository : ITypeRepository {
        private IDatabase db;
        private readonly IConfiguration _config;

        public TypeRepository(IConfiguration config) {
            _config = config;
            string connectionString = _config.GetConnectionString("CacheConnection");

            var cm = ConnectionMultiplexer.Connect(connectionString);
            db = cm.GetDatabase();
        }

        public List<CarTypes> GetTypes() {
            if (db.KeyExists("type-options")) {
                string menusSerialized = db.StringGet("type-options");
                var list = JsonConvert.DeserializeObject<List<CarTypes>>(menusSerialized);
                return list;
            } else {
                return new List<CarTypes>();
            }
        }

        public void UpserType(CarTypes t) {
            var originalList = GetTypes();
            var existentType = originalList.SingleOrDefault(x => x.ServiceName == t.ServiceName);
            if (existentType != null) {
                //update part
                existentType.ServiceName = t.ServiceName;
                existentType.ServicePrice = t.ServicePrice;

            } else {
                //insert it
                originalList.Add(t);
            }

            var serializedTypes = JsonConvert.SerializeObject(originalList);
            db.StringSet("type-options", serializedTypes);
        }
    }
}
