﻿using CloudAssignment.DataAccess.Interfaces;
using CloudAssignment.Models.Domain;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Repositries {
    public class PubSubRepository : IPubSubRepository {
        public enum Category { Economy, Business, Luxury };

        string projectId;

        public PubSubRepository(IConfiguration config) {
            projectId = config.GetSection("AppSettings").GetSection("ProjectId").Value;
        }

        public void PublishBooking(Booking b, string email, string serviceType) {
            TopicName topicName = TopicName.FromProjectTopic(projectId, serviceType + "Topic");

            Task<PublisherClient> t = PublisherClient.CreateAsync(topicName);
            t.Wait();
            PublisherClient publisher = t.Result;

            var myOnTheFlyObject = new { Email = email, Booking = b };

            string myOnTheFlyObject_serialized = JsonConvert.SerializeObject(myOnTheFlyObject);

            var publishMessage = new PubsubMessage {
                Data = ByteString.CopyFromUtf8(myOnTheFlyObject_serialized),
                Attributes =
                {
                    { "Category", serviceType }
                }
            };

            Task<String> t2 = publisher.PublishAsync(publishMessage);
            t2.Wait();
            string message = t2.Result;

        }


        public List<Booking> PullServices(string cat) {
            List<Booking> myTexts = new List<Booking>();
            SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription(projectId, cat + "Topic-sub");
            SubscriberServiceApiClient subscriberClient = SubscriberServiceApiClient.Create();

            string text = "";

            try {
                // Pull messages from server,
                // allowing an immediate response if there are no messages.
                PullResponse response = subscriberClient.Pull(subscriptionName, returnImmediately: true, maxMessages: 1); //one message at a time

                if (response.ReceivedMessages.Count > 0) {
                    /*foreach (ReceivedMessage msg in response.ReceivedMessages)
                    {*/
                    var mes = response.ReceivedMessages.FirstOrDefault();
                    if (mes != null) {
                        text = mes.Message.Data.ToStringUtf8();

                        dynamic myDeserializedData = JsonConvert.DeserializeObject(text);
                        Booking b = new Booking();
                        b.AckId = mes.AckId;
                        b.Email = myDeserializedData.Email;
                        b.Type = myDeserializedData.Booking.Type;
                        b.AddressFrom = myDeserializedData.Booking.AddressFrom;
                        b.AddressTo = myDeserializedData.Booking.AddressTo;

                        myTexts.Add(b);
                    }
                }

            } catch (RpcException ex) when (ex.Status.StatusCode == StatusCode.Unavailable) {
                // UNAVAILABLE due to too many concurrent pull requests pending for the given subscription.
            }

            return myTexts;
        }

        public void AcknowledgeMessage(string AckId, string cat) {
            SubscriptionName subscriptionName = SubscriptionName.FromProjectSubscription(projectId, cat + "Topic-sub");
            SubscriberServiceApiClient subscriberClient = SubscriberServiceApiClient.Create();

            subscriberClient.Acknowledge(subscriptionName, new List<string>() { AckId });

        }



    }
}
