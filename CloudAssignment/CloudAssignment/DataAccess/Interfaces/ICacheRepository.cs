﻿using CloudAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface ICacheRepository {
        List<Menu> GetMenus();
        void UpserMenu(Menu m);
    }
}
