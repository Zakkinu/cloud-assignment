﻿using CloudAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface ITypeRepository {
        List<CarTypes> GetTypes();
        void UpserType(CarTypes t);
    }
}
