﻿using CloudAssignment.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CloudAssignment.DataAccess.Interfaces {
    public interface ISubbedBookingsRepository {
        void AddConfirmedBooking(SubbedBookings sb);
        IQueryable<SubbedBookings> GetBookings();
    }
}
